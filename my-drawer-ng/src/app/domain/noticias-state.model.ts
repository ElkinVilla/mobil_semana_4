import { Injectable } from "@angular/core";
import { Action } from "@ngrx/store";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Observable, of } from "rxjs";
import { map } from "rxjs/operators";

export class Noticia {
    constructor(public titulo: string) {}
}

// ESTADO
export interface NoticiasState {
    items: Array<Noticia>;
    lecturas: Array<string>;
    sugerida: Noticia;
}

export function intializeNoticiasState() {
  return {
    items: [],
    lecturas: [],
    sugerida: null
  };
}

// ACCIONES
export enum NoticiasActionTypes {
    INIT_MY_DATA = "[Noticias] Init My Data",
    NUEVA_NOTICIA = "[Noticias] Nueva",
    SUGERIR_NOTICIA = "[Noticias] Sugerir",
    LEER_NOTICIA = "[Noticias] Leer"
}

// tslint:disable-next-line:max-classes-per-file
export class InitMyDataAction implements Action {
    type = NoticiasActionTypes.INIT_MY_DATA;
    constructor(public titulares: Array<string>) {}
}

// tslint:disable-next-line:max-classes-per-file
export class NuevaNoticiaAction implements Action {
  type = NoticiasActionTypes.NUEVA_NOTICIA;
  constructor(public noticia: Noticia) {}
}

// tslint:disable-next-line:max-classes-per-file
export class SugerirAction implements Action {
  type = NoticiasActionTypes.SUGERIR_NOTICIA;
  constructor(public noticia: Noticia) {}
}

// tslint:disable-next-line:max-classes-per-file
export class LeerAhoraAction implements Action {
    type = NoticiasActionTypes.LEER_NOTICIA;
    constructor(public lectura: string) {}
}

export type NoticiasActions = InitMyDataAction | NuevaNoticiaAction | SugerirAction | LeerAhoraAction;

// REDUCERS
export function reducerNoticias(
  state: NoticiasState,
  action: NoticiasActions
): NoticiasState {
  switch (action.type) {
    case NoticiasActionTypes.INIT_MY_DATA: {
      const titulares: Array<string> = (action as InitMyDataAction).titulares;

      return {
          ...state,
          items: titulares.map((t) => new Noticia(t))
        };
    }
    case NoticiasActionTypes.NUEVA_NOTICIA: {
      return {
          ...state,
          items: [...state.items, (action as NuevaNoticiaAction).noticia]
        };
    }
    case NoticiasActionTypes.SUGERIR_NOTICIA: {
        return {
          ...state,
          sugerida: (action as SugerirAction).noticia
        };
    }
      case NoticiasActionTypes.LEER_NOTICIA: {
          const lectura: string = (action as LeerAhoraAction).lectura;

          if (state.lecturas.indexOf(lectura) === -1) {
              return {
                  ...state,
                  lecturas: [...state.lecturas, lectura]
              };
          }
      }
  }

  return state;
}

// EFFECTS
// tslint:disable-next-line:max-classes-per-file
@Injectable()
export class NoticiasEffects {
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
    ofType(NoticiasActionTypes.NUEVA_NOTICIA),
    map((action: NuevaNoticiaAction) => new SugerirAction(action.noticia))
  );

  constructor(private actions$: Actions) {}
}
