import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { NoticiasService } from "~/app/domain/noticias.service";
import * as Toast from "nativescript-toasts";
import { Store } from "@ngrx/store";
import { AppState } from "~/app/app.module";
import { LeerAhoraAction} from "~/app/domain/noticias-state.model";

@Component({
    selector: "Favoritos",
    moduleId: module.id,
    templateUrl: "./favoritos.component.html"
})
export class FavoritosComponent implements OnInit {

    favoritos: Array<string> = [];

    constructor(private noticiasService: NoticiasService,
                private store: Store<AppState>
    ) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.listarFavoritos();
    }

    listarFavoritos() {
        this.noticiasService.favs().then((r: any) => {
            console.log("favoritos: " + JSON.stringify(r));
            this.favoritos = r;
        }, (e) => {
            console.log("error listar favs " + e);
            Toast.show({text: "Error en listar favs", duration: Toast.DURATION.SHORT});
        });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onFavoritoItemTap(args) {
        this.store.dispatch(new LeerAhoraAction(args.view.bindingContext));
        Toast.show({text: "Se agregó a leer ahora", duration: Toast.DURATION.SHORT});
    }
}
