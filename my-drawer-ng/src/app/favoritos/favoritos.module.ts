import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { FavoritosRoutingModule } from "./favoritos-routing.module";
import { FavoritosComponent } from "./favoritos.component";
import { CommonModule } from "@angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

@NgModule({
  imports: [
    NativeScriptCommonModule,
    FavoritosRoutingModule,
    CommonModule,
    NativeScriptFormsModule
  ],
  declarations: [
    FavoritosComponent
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class FavoritosModule { }
