import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListadoComponent } from "./listado.component";
import { DetalleComponent } from "./detalle/detalle.component";

const routes: Routes = [
    { path: "", component: ListadoComponent },
    { path: "listado", component: ListadoComponent },
    { path: "detalle", component: DetalleComponent },
    { path: "detalle/:id", component: DetalleComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListadoRoutingModule { }
