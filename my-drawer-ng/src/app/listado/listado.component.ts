import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { RouterExtensions } from "nativescript-angular/router";
import { Opcion } from "~/app/models/opcion";
import { OpcionesService } from "~/app/domain/opciones.service";
import { View, Color } from "tns-core-modules/ui/core/view/view";
import * as dialogs from "tns-core-modules/ui/dialogs";
import * as Toast from "nativescript-toasts";

@Component({
  selector: "ns-listado",
  templateUrl: "./listado.component.html"
})
export class ListadoComponent implements OnInit {

    opcionesFiltradas: Array<Opcion> = [];
    // @ts-ignore
    @ViewChild("layout") layout: ElementRef;
    // @ts-ignore
    // @ViewChild("imagen") imagen: ElementRef;
    @ViewChildren("imagenes") imagenes: QueryList<ElementRef>;

    constructor(private opcionesService: OpcionesService, private routerExtensions: RouterExtensions) {
    }

    ngOnInit(): void {
        this.opcionesFiltradas = this.opcionesService.listar();
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(x): void {
        const imagen = <View>this.imagenes.toArray()[x.index].nativeElement;
        imagen.originX = 0.5; // default 0.5 (center), 0 is most left, 1 is most right
        imagen.originY = 0.5; // default 0.5 (middle), 0 is top, 1 is bottom
        imagen.animate({
            rotate: 360, // will take into account originX and originY
            duration: 1000,
            delay: 150
        }).then (() => {
            this.routerExtensions.navigate(["/listado/detalle" , this.opcionesFiltradas[x.index].id], {
                transition: {
                    name: "fade"
                }
            });
        });
    }

    onPull(e) {
        console.log(e);
        const pullRefresh = e.object;
        // setTimeout(() => {
        this.opcionesService.agregar();
        this.opcionesFiltradas = this.opcionesService.listar();
        pullRefresh.refreshing = false;
        // }, 1000);
    }

    buscarAhora(s: string) {
        this.opcionesFiltradas = this.opcionesService.buscar(s);
        const layout = <View>this.layout.nativeElement;
        layout.animate({
           backgroundColor: new Color("blue"),
           duration: 300,
           delay: 150
        }).then(() => layout.animate({
            backgroundColor: new Color("white"),
            duration: 300,
            delay: 150
        }));
    }

    categoria(opc: Opcion) {
        const actionOptions = {
            title: "Modificar Categoría",
            message: "Elija la nueva categoría",
            cancelButtonText: "Cancelar",
            actions: ["1 Estrella", "2 Estrellas", "3 Estrellas", "4 Estrellas", "5 Estrelllas"],
            cancelable: true // Android only
        };
        dialogs.action(actionOptions)
            .then((nuevaCategoria) => {
                console.log("resultados: " + nuevaCategoria);
                if (nuevaCategoria) {
                    opc.categoria = nuevaCategoria;
                    this.opcionesService.actualizar(opc);
                    const toastOptions: Toast.ToastOptions = {
                        text: "Se actualizó la categoría a: " + nuevaCategoria,
                        duration: Toast.DURATION.SHORT
                    };
                    Toast.show(toastOptions);
                }
            });
    }


}
