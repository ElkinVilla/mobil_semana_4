import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ListadoRoutingModule } from './listado-routing.module';
import { ListadoComponent } from './listado.component';
import { DetalleComponent } from './detalle/detalle.component';
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { BuscarComponent } from "~/app/listado/buscar.component";

@NgModule({
  declarations: [
      ListadoComponent,
      DetalleComponent,
      BuscarComponent
  ],
  imports: [
    NativeScriptCommonModule,
    ListadoRoutingModule,
    NativeScriptFormsModule
  ],
  schemas: [
      NO_ERRORS_SCHEMA
  ]
})
export class ListadoModule { }
