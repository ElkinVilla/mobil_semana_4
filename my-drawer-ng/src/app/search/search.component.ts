import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { NoticiasService } from "~/app/domain/noticias.service";
import * as Toast from "nativescript-toasts";
import { Store } from "@ngrx/store";
import { AppState } from "~/app/app.module";
import { Noticia, NuevaNoticiaAction } from "~/app/domain/noticias-state.model";
import * as SocialShare from "nativescript-social-share";

@Component({
    selector: "Search",
    moduleId: module.id,
    templateUrl: "./search.component.html"
})
export class SearchComponent implements OnInit {

    // noticias: Array<string> = [];
    resultados: Array<string> = [];
    // favoritos: Array<string> = [];

    constructor(private noticiasService: NoticiasService,
                private store: Store<AppState>
    ) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
        // this.noticiasService.agregar("Hola 1");
        // this.noticiasService.agregar("Hola 2");
        // this.noticiasService.agregar("Hola 3");
        // this.noticias = this.noticiasService.buscar();
        // console.dir(this.noticias);
        // this.listarFavoritos();
        this.store.select((state) => state.noticias.sugerida)
            .subscribe((data) => {
               const f = data;
               if (f != null) {
                   Toast.show({text: "Sugerimos leer: " + f.titulo, duration: Toast.DURATION.SHORT})
               }
            });
    }

    // listarFavoritos() {
    //     this.noticiasService.favs().then((r: any) => {
    //         console.log("favoritos: " + JSON.stringify(r));
    //         this.favoritos = r;
    //     }, (e) => {
    //         console.log("error listar favs " + e);
    //         Toast.show({text: "Error en listar favs", duration: Toast.DURATION.SHORT});
    //     });
    // }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args): void {
        console.dir(args);
        console.log(this.resultados[args.index]);
        // this.noticiasService.agregar(this.resultados[args.index]).then((resp) => this.listarFavoritos());
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
        Toast.show({text: "Se agregó a favoritos", duration: Toast.DURATION.SHORT});
    }

    buscarAhora(s: string) {
        // this.resultados = this.noticias.filter((n) => n.indexOf(s) >= 0);
        console.log("buscarAhora " + s);
        this.noticiasService.buscar(s).then((r: any) => {
            console.log("resultados buscarAhora: " + JSON.stringify(r));
            this.resultados = r;
        }, (e) => {
            console.log("error buscarAhora " + e);
            Toast.show({text: "Error en la búsqueda", duration: Toast.DURATION.SHORT});
        });
    }

    // onFavoritoItemTap(args) {
    //     this.store.dispatch(new LeerAhoraAction(args.view.bindingContext));
    // }

    onLongPress(s) {
        SocialShare.shareText(s, "Asunto compartido desde el curso");
    }
}
