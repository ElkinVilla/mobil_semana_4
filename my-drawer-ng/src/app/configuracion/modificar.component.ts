import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import * as Toast from "nativescript-toasts";

@Component({
  selector: "Modificar",
  template: `
      <FlexboxLayout flexDirection="row">
          <Label text="Nuevo nombre de usuario:" color="blue" fontSize="14" margin="3" backgroundColor="lightblue"></Label>
          <TextField #texto="ngModel" [(ngModel)]="textFieldValue" hint="Ingresar texto..." required minlen="4" fontSize="16"></TextField>
          <Label *ngIf="texto.hasError('required')" text="*"></Label>
          <Label *ngIf="!texto.hasError('required') && texto.hasError('minlen')" text="4+"></Label>
      </FlexboxLayout>
      <Button text="Guardar" (tap)="onButtonTap()" *ngIf="texto.valid"></Button>
  `
})
export class ModificarComponent implements OnInit {

    textFieldValue: string = "";
    @Output() modificar: EventEmitter<string> = new EventEmitter<string>();
    @Input() inicial: string;

    constructor() { }

    ngOnInit(): void {
      this.textFieldValue = this.inicial;
    }

    onButtonTap(): void {
        console.log(this.textFieldValue);
        if (this.textFieldValue.length > 3) {
            this.modificar.emit(this.textFieldValue);
        } else {
            Toast.show({text: "Se requiere mínimo 4 caracteres", duration: Toast.DURATION.SHORT});
        }
    }
}
